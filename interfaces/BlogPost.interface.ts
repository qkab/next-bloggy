interface IDescription {
    markdown: string
}
export interface IBlogPost {
    title: string
    slug: string
    description: IDescription[]
}

export interface IBlogPostProps {
    posts: IBlogPost[]
}

export interface IPostProps {
    post: IBlogPost
}