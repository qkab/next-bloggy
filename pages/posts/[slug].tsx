import { gql } from 'graphql-request'
import { GetStaticPaths, GetStaticProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import React from 'react'
import ReactMarkdown from 'react-markdown'
import { graphcms } from '../../graphcms'
import { IBlogPost, IPostProps } from '../../interfaces/BlogPost.interface'

const Post: React.FC<IPostProps> = ({ post }) => {
    return (
        <div>
            <Link href="/">
                <a>Go Back</a>
            </Link>
            <Head>
                <title>{post.slug}</title>
                <meta name="description" content={post.title} />
            </Head>
            <h2>{post.title}</h2>
            <ReactMarkdown>{post.description[0].markdown}</ReactMarkdown>
        </div>
    )
}

export const getStaticPaths: GetStaticPaths = async () => {
    const QUERY = gql`
        {
            blogPosts {
                title
                slug
                description {
                    markdown
                }
            }
        }
    `

    const { blogPosts } = await graphcms.request(QUERY)
    const paths = blogPosts.map(({ slug }: IBlogPost) => ({ params: { slug }}))

    return { paths, fallback: false }
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
    const QUERY = gql`
        query BlogPostQuery($slug: String!){
            blogPost(where: { slug: $slug }) {
                title
                slug
                description {
                    markdown
                }
            }
        }
    `

    const { blogPost } = await graphcms.request(QUERY, { slug: params!.slug })
    return {
        props: { post: blogPost }
    }
}

export default Post
