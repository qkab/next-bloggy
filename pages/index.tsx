import { gql } from 'graphql-request'
import type { GetStaticProps, NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import ReactMarkdown from 'react-markdown'

import { graphcms } from '../graphcms'
import { IBlogPostProps } from '../interfaces/BlogPost.interface'

import styles from '../styles/Home.module.css'


const Home: NextPage<IBlogPostProps> = ({ posts }) => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Bloggy</title>
        <meta name="description" content="Best Blog app" />
        <meta name="keywords" content="Nextjs, Blog app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <a href="#">Bloggy</a>
        </h1>

        {posts.map((post) => (
          <div className={styles.grid}>
            <Link href={`/posts/${post.slug}`}>
              <a className={styles.card}>
                <h2>{post.title}</h2>
                <ReactMarkdown>{post.description[0].markdown}</ReactMarkdown>
              </a>
            </Link>
          </div>
        ))}
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  const QUERY = gql`
    {
      blogPosts {
        title
        slug
        description {
          markdown
        }
      }
    }
  `

  const { blogPosts } = await graphcms.request(QUERY)

  return {
    props: { posts: blogPosts }
  }
}
export default Home
