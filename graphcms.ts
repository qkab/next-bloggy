import { GraphQLClient } from "graphql-request"

const API_URL = process.env.NEXT_PUBLIC_API || ''
const graphcms = new GraphQLClient(API_URL)

export { graphcms }